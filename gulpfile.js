var sass = require('gulp-sass');
var gulp = require("gulp");

gulp.task('sass', function(){
    return gulp.src('css/*.sass')
        .pipe(sass()) // Using gulp-sass
        .pipe(gulp.dest('./css/'))
});

gulp.task('watch', function(){
    gulp.watch('css/*.sass', gulp.series('sass'));
    // Other watchers
})