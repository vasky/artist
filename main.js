const blackSreen = document.querySelector("#black-screen")
const mobileMenu = document.querySelector(".mobile-menu__body")
const burgetButton = document.querySelector(".burger__icon")

const exampleWorks = document.querySelectorAll(".example__work")
const partners = document.querySelectorAll(".partner")

const showAllExamples = document.querySelector('.btn__showAllExamples')
const showAllPartners = document.querySelector('.btn__showAllPartners')
const awards = document.querySelectorAll('.competition__image')
const windowAwards = document.querySelector('#mobileList__window')

blackSreen.addEventListener("click", () => {
    mobileMenu.style.display = 'none'
    blackSreen.style.display = 'none'
})
burgetButton.addEventListener( "click", () => {
    mobileMenu.style.display = 'block'
    blackSreen.style.display = 'block'

})
awards.forEach( award => {
    award.addEventListener( 'click', () => {
        if( award.dataset.numberAward === "1"){
            windowAwards.innerHTML = `<ul class="descriptionOnYears">
                            <li class="descriptionOnYears__item">
                                1997г. — диплом лауреата, видеоролик «Обходчик»
                            </li>
                            <li class="descriptionOnYears__item">
                                2003г. — диплом лауреата, видеоролик «Дурак»
                            </li>
                            <li class="descriptionOnYears__item">
                                2005г. — диплом за I место, видеоролики «Казарма», «Трусы», «Футбол», «8цветков»
                            </li>
                            <li class="descriptionOnYears__item">
                                2009г. — диплом за III место, видеоролики «Березень»
                            </li>
                        </ul>`
        }else if( award.dataset.numberAward === "2" ){
            windowAwards.innerHTML = `<ul class="descriptionOnYears">
                            <li class="descriptionOnYears__item">
                                2002г. — диплом за II место, видеоролик «Бабульки»
                            </li>
                            <li class="descriptionOnYears__item">
                                2003г. — диплом за III место, видеоролики «Кристал разгуляй 20», «ПМТ»
                            </li>
                            <li class="descriptionOnYears__item">
                                2003г. — диплом за II место, видеоролики «Казарма», «Молния гномы 25»
                            </li>
                            <li class="descriptionOnYears__item">
                                2003г. — диплом за I место, видеоролик «Альфа-база 36»
                            </li>
                            <li class="descriptionOnYears__item">
                                2009г. — диплом за III место, видеоролики «Березень»
                            </li>
                        </ul>`
        }else{
            windowAwards.innerHTML = `<ul class="descriptionOnYears">
                            <li class="descriptionOnYears__item">
                                2004г. — диплом финалиста, видеоролик «Трусы»
                            </li>
                            <li class="descriptionOnYears__item">
                                2006г. — диплом за III место, видеоролик «Шеврон»
                            </li>
                        </ul>`
        }

    })
})
showAllExamples.addEventListener( 'click', () => {
    clickBtnShowMore(showAllExamples, exampleWorks, 'only examples', 'Показать часть примеров', 'Показать все примеры')
})
showAllPartners.addEventListener("click", ()=>{
    clickBtnShowMore(showAllPartners, partners, 'only partners', 'Показать часть партнеров', 'Показать всех партнеров')
})
document.addEventListener("DOMContentLoaded", ()=>{
    currentShowExamples()
});

window.addEventListener( 'resize', ()=>{
    currentShowExamples()
})
function clickBtnShowMore(btn, elements, whatShow, textWhenOpen, textWhenClose) {
    btn.classList.toggle("active")
    if( btn.classList.contains('active')){
        btn.textContent = textWhenOpen
        showAllExamplesFn(elements)
    }else{
        btn.textContent = textWhenClose
        currentShowExamples(whatShow)
    }
}
function currentShowExamples(section = false) {
    const sw = screen.width
    if( sw < 768) {
        switch(section){
            case 'only examples':
                currentBlockSize(exampleWorks, sw)
                break;
            case 'only partners':
                currentBlockSize(partners, sw)
                break;
            default:
                currentBlockSize(exampleWorks, sw)
                currentBlockSize(partners, sw)
        }
        // currentBlockSize(exampleWorks, sw)
        // currentBlockSize(partners, sw)
    }else{
        showAllExamplesFn(exampleWorks)
        showAllExamplesFn(partners)
    }
}
function currentBlockSize(blocks, sw){
    blocks.forEach( block => {
        if( block.dataset.setShow !== 'always' && block.dataset.setShow >= sw || !block.dataset.setShow){
            block.classList.add('hide')
        }else{
            block.classList.remove('hide')
        }
    })
}
function showAllExamplesFn(elements) {

    elements.forEach( ex => {
        ex.classList.remove('hide')
    })
}